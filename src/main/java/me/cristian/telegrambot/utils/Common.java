package me.cristian.telegrambot.utils;

import me.cristian.telegrambot.emojis.Emoji;

public class Common {

    public static String getCloseCommand() {
        return Emoji.ERROR.toString() + " Close";
    }

    public static String getHelpCommand() {
        return Emoji.QUESTION.toString() + " Help";
    }

    public static String getInfoCommand() {
        return Emoji.QUESTION.toString() + " Info";
    }

    public static String getDrawCommand() {
        return Emoji.PENCIL.toString() + " Draw";
    }

}
