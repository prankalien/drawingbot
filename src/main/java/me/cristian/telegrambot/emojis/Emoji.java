package me.cristian.telegrambot.emojis;

public enum Emoji {

    ERROR('\u274c'),
    QUESTION('\u2754'),
    PENCIL('\u270f');

    Character unicode;

    Emoji(final Character unicode) {
        this.unicode = unicode;
    }

    @Override
    public String toString() {
        return String.valueOf(unicode);
    }

}
