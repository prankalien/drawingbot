package me.cristian.telegrambot.commands;

import me.cristian.telegrambot.Configuration;
import me.cristian.telegrambot.keyboards.HelpKeyboard;
import me.cristian.telegrambot.utils.Common;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class HelpCommand extends TelegramLongPollingBot {

    @Override
    public void onUpdateReceived(final Update update) {

        if (!update.hasMessage() && !update.getMessage().hasText()) return;

        if (!update.getMessage().getText().equals(Common.getHelpCommand())) return;

        final Message nativeMessage = update.getMessage();

        final SendMessage message = new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .enableMarkdown(true);

        CommandsManager.addToKeyboardQueue(nativeMessage.getFrom().getId());

        message.setText("Cosa ti serve ?");
        message.setReplyMarkup(HelpKeyboard.getHelpKeyboard());

        try {
            execute(message);
        } catch (final TelegramApiException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public String getBotUsername() {
        return Configuration.USER;
    }

    @Override
    public String getBotToken() {
        return Configuration.TOKEN;
    }

}
