package me.cristian.telegrambot.commands;

import java.util.ArrayList;
import java.util.List;

public class CommandsManager {

    private static final List<Integer> keyboardQueue = new ArrayList<>();

    public static List<Integer> getKeyboardQueue() {
        return keyboardQueue;
    }

    public static void addToKeyboardQueue(final int id) {
        if (keyboardQueue.contains(id)) return;
        keyboardQueue.add(id);
    }

    public static void removeFromKeyboardQueue(final int id) {
        if (!keyboardQueue.contains(id)) return;
        keyboardQueue.remove(Integer.valueOf(id));
    }

    public static boolean isInKeyboardQueue(final int id) {
        return keyboardQueue.contains(id);
    }

}
