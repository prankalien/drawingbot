package me.cristian.telegrambot.commands;

import me.cristian.telegrambot.Configuration;
import me.cristian.telegrambot.keyboards.MainKeyboard;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class StartCommand extends TelegramLongPollingBot {

    @Override
    public void onUpdateReceived(final Update update) {

        if (!update.hasMessage() && !update.getMessage().hasText()) return;

        if (!update.getMessage().getText().equals("/start")) return;

        final long chatID = update.getMessage().getChatId();
        final Message nativeMessage = update.getMessage();

        final SendMessage message = new SendMessage()
                .setChatId(chatID)
                .enableMarkdown(true);

        message.setText("Benvenuto " + nativeMessage.getFrom().getFirstName() + "!\nCome posso aiutarti ?");
        message.setReplyMarkup(MainKeyboard.getMainKeyboard());

        try {
            execute(message);
        } catch (final TelegramApiException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public String getBotUsername() {
        return Configuration.USER;
    }

    @Override
    public String getBotToken() {
        return Configuration.TOKEN;
    }

}
