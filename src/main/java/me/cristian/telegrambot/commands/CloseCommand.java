package me.cristian.telegrambot.commands;

import me.cristian.telegrambot.Configuration;
import me.cristian.telegrambot.utils.Common;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class CloseCommand extends TelegramLongPollingBot {

    @Override
    public void onUpdateReceived(final Update update) {

        if (!update.hasMessage() && !update.getMessage().hasText()) return;

        if (!update.getMessage().getText().equals(Common.getCloseCommand())) return;

        final Message nativeMessage = update.getMessage();

        if (!CommandsManager.isInKeyboardQueue(nativeMessage.getFrom().getId())) return;

        CommandsManager.removeFromKeyboardQueue(nativeMessage.getFrom().getId());

        final SendMessage message = new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .enableMarkdown(true);

        final ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();

        message.setReplyMarkup(replyKeyboardRemove);
        message.setText("Menu Closed...");

        try {
            execute(message);
        } catch (final TelegramApiException ex) {
            ex.printStackTrace();
        }

    }

    public String getBotUsername() {
        return Configuration.USER;
    }

    public String getBotToken() {
        return Configuration.TOKEN;
    }

}
