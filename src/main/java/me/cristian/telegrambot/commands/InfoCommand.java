package me.cristian.telegrambot.commands;

import me.cristian.telegrambot.Configuration;
import me.cristian.telegrambot.utils.Common;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class InfoCommand extends TelegramLongPollingBot {

    @Override
    public void onUpdateReceived(final Update update) {

        if (!update.hasMessage() && !update.getMessage().hasText()) return;

        if (!update.getMessage().getText().equals(Common.getInfoCommand())) return;

        final SendMessage message = new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .enableMarkdown(true)
                .enableHtml(true);

        message.setText("<b>INFO</b>\nVersion: " + Configuration.VERSION + "\nDeveloper: <a href=\"tg://user?id=399249712\">@Mr_Cookies</a>");

        try {
            execute(message);
        } catch (final TelegramApiException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public String getBotUsername() {
        return Configuration.USER;
    }

    @Override
    public String getBotToken() {
        return Configuration.TOKEN;
    }

}

