package me.cristian.telegrambot;

import me.cristian.telegrambot.commands.CloseCommand;
import me.cristian.telegrambot.commands.HelpCommand;
import me.cristian.telegrambot.commands.InfoCommand;
import me.cristian.telegrambot.commands.StartCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class Core {

    private static final Logger LOGGER = LoggerFactory.getLogger(Core.class);

    public static void main(final String[] args) {

        ApiContextInitializer.init();

        final TelegramBotsApi api = new TelegramBotsApi();

        try {

            api.registerBot(new StartCommand());
            api.registerBot(new CloseCommand());
            api.registerBot(new HelpCommand());
            api.registerBot(new InfoCommand());

        } catch (final TelegramApiException e) {
            LOGGER.error("Error while registering the classes", e);
        }

    }

}
