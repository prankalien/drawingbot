package me.cristian.telegrambot.keyboards;

import me.cristian.telegrambot.utils.Common;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class HelpKeyboard {

    public static ReplyKeyboardMarkup getHelpKeyboard() {

        final ReplyKeyboardMarkup reply = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setOneTimeKeyboard(false);

        final List<KeyboardRow> keyboard = new ArrayList<>();

        final KeyboardRow firstRow = new KeyboardRow();
        final KeyboardRow secondRow = new KeyboardRow();

        firstRow.add(Common.getInfoCommand());
        secondRow.add(Common.getCloseCommand());

        keyboard.add(firstRow);
        keyboard.add(secondRow);

        reply.setKeyboard(keyboard);

        return reply;
    }

}
